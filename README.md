# DesigniteGitVersionAnalyzer

A Batch Script that use the Java version of Designite to analyze different commits of a git project.

## Name
Designite Git Version Analyzer

## Description
This project is usefull when you need to analyze different version of a git project. You can easely choose the commits to checkout and analyze the project in those specific versions.

## Requirements
* Java version of designite. (https://www.designite-tools.com/designitejava)
* File txt that contains a list of commit tags (one tag per line), in the folder where the DesigniteJava.jar file is located. The file.txt must have the same name of the project that must be analyzed.
```batch
f50582306a758e37089ff6a4ec948361b032e71c
a99e24a8464eb6ad0c38f23536142acd693225a1
g19adb5d840e723631ef1030f689f5492a716361
f9d48997177697fbe0db24a45a8f6492268413f3
```

## Usage
Copy the DesigniteGitVersionAnalyzer.bat file in the folder where the DesigniteJava.jar file is located. Run the following
command:
```batch
.\DesigniteGitVersionAnalyzer.bat <Projects path> <Output path> <Project name>
```
* **Projects path:** The path where is located the project to analyze.
* **Output path:** The path where the output will be saved.
* **Project name:** The name of the project to analyze.

## Authors and acknowledgment
### Authors: 
* Andrea Titaro (andre.titaro@gmail.com).
* Mauro Sonzogni (maurosonzo1997@gmail.com).

## License
Designite License.

